FROM node:20.11.1-alpine as base
WORKDIR /build

COPY package.json package-lock.json ./
RUN npm ci

COPY ./ ./

RUN npm run build

FROM node:20.11.1-alpine
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm ci --production
COPY --from=base /build/dist ./dist

CMD ["npm", "run", "start:prod"]