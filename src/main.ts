import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { buildSwagger } from './swagger.helpers';
import { Logger } from 'nestjs-pino';
import { HttpConfig } from './core/config/http.config';
import { initializeTransactionalContext } from 'typeorm-transactional';
import { getBotToken } from 'nestjs-telegraf';
import { Telegraf } from 'telegraf';

async function bootstrap() {
  initializeTransactionalContext();
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });
  app.useLogger(app.get(Logger));
  buildSwagger(app);
  const bot: Telegraf = app.get(getBotToken());
  app.use(bot.webhookCallback('/secret-path'));

  const httpConfig = app.get(HttpConfig);
  await app.listen(httpConfig.port);
}

void bootstrap();
