import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUsers1710193581584 implements MigrationInterface {
  name = 'CreateUsers1710193581584';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "users" (
        "id" bigint NOT NULL, 
        "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
        "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
        "firstName" text NOT NULL, 
        "lastName" text, 
        "username" text, 
        "languageCode" text, 
        "isPremium" boolean, 
        CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id")
      )`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "users"`);
  }
}
