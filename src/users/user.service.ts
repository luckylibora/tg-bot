import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { Repository } from 'typeorm';
import { UpsertUserDto } from './dto/upsert-user.dto';
import { UserDto } from './dto/user.dto';
import { Transactional } from 'typeorm-transactional';
import { Nullable } from '../core/types/nullable.type';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async getAdminById(id: string): Promise<Nullable<UserDto>> {
    const userEntity = await this.userRepository.findOneBy({
      id,
      isAdmin: true,
    });

    if (!userEntity) {
      return null;
    }

    return new UserDto(userEntity);
  }

  @Transactional()
  async upsert(upsertUserDto: UpsertUserDto): Promise<UserDto> {
    const userEntity = this.userRepository.create(upsertUserDto);
    await this.userRepository.save(userEntity);

    return new UserDto(userEntity);
  }
}
