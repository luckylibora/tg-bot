import { Nullable } from '../../core/types/nullable.type';

export class UpsertUserDto {
  id: string;

  firstName: string;

  lastName: Nullable<string>;

  username: Nullable<string>;

  languageCode: Nullable<string>;

  isPremium: Nullable<boolean>;

  constructor(data: Partial<UpsertUserDto>) {
    Object.assign(this, data);
  }
}
