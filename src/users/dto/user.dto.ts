import { Nullable } from '../../core/types/nullable.type';

export class UserDto {
  id: string;

  createdAt: Date;

  updatedAt: Date;

  firstName: string;

  lastName: Nullable<string>;

  username: Nullable<string>;

  languageCode: Nullable<string>;

  isPremium: Nullable<boolean>;

  isAdmin: boolean;

  constructor(data: Partial<UserDto>) {
    Object.assign(this, data);
  }
}
