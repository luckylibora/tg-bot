import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Nullable } from '../core/types/nullable.type';

@Entity('users')
export class UserEntity {
  @PrimaryColumn('bigint')
  id: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
  })
  updatedAt: Date;

  @Column({ type: 'text' })
  firstName: string;

  @Column({ type: 'text', nullable: true })
  lastName: Nullable<string>;

  @Column({ type: 'text', nullable: true })
  username: Nullable<string>;

  @Column({ type: 'text', nullable: true })
  languageCode: Nullable<string>;

  @Column({ type: 'boolean', nullable: true })
  isPremium: Nullable<boolean>;

  @Column({ type: 'boolean', default: false })
  isAdmin: boolean;
}
