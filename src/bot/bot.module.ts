import { Module } from '@nestjs/common';
import { BotService } from './bot.service';
import { UserModule } from '../users/user.module';
import { BotConfig } from './bot.config';

@Module({
  imports: [UserModule],
  providers: [BotService, BotConfig],
})
export class BotModule {}
