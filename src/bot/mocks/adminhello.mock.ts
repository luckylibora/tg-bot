export const adminhelloMock = {
  message_id: 89,
  from: {
    id: 68954753,
    is_bot: false,
    first_name: 'Alex',
    last_name: 'Vasilev',
    username: 'luckylibora',
    language_code: 'en',
  },
  chat: {
    id: 68954753,
    first_name: 'Alex',
    last_name: 'Vasilev',
    username: 'luckylibora',
    type: 'private',
  },
  date: 1710284084,
  text: '/adminhello 68954753 hallo hallo',
  entities: [
    {
      offset: 0,
      length: 11,
      type: 'bot_command',
    },
    {
      offset: 12,
      length: 8,
      type: 'phone_number',
    },
  ],
  args: ['68954753', 'hallo', 'hallo'],
};
