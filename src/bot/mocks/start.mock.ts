export const startMock = {
  message: {
    message_id: 85,
    from: {
      id: 68954753,
      is_bot: false,
      first_name: 'Alex',
      last_name: 'Vasilev',
      username: 'luckylibora',
      language_code: 'en',
    },
    chat: {
      id: 68954753,
      first_name: 'Alex',
      last_name: 'Vasilev',
      username: 'luckylibora',
      type: 'private',
    },
    date: 1710283283,
    text: '/start',
    entities: [
      {
        offset: 0,
        length: 6,
        type: 'bot_command',
      },
    ],
  },
  args: ['68954753', 'hallo', 'hallo'],
};
