import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class BotConfig {
  constructor(private configService: ConfigService) {}

  get webAppUrl(): string {
    return this.configService.get<string>('WEB_APP_URL')!;
  }
}
