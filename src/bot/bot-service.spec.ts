/* eslint-disable @typescript-eslint/unbound-method */
import { BotService } from './bot.service';
import { UserService } from '../users/user.service';
import { TestBed } from '@automock/jest';
import { MessageContext } from './message-context.type';
import { startMock } from './mocks/start.mock';
import { adminhelloMock } from './mocks/adminhello.mock';

describe('BotService', () => {
  let botService: BotService;
  let userService: jest.Mocked<UserService>;

  beforeAll(() => {
    const { unit, unitRef } = TestBed.create(BotService).compile();

    botService = unit;
    userService = unitRef.get(UserService);
  });

  describe('/start', () => {
    it('should upsert user to db', async () => {
      const reply = jest.fn();
      await botService.start({
        ...startMock,
        reply,
      } as unknown as MessageContext);

      expect(userService.upsert).toHaveBeenCalledWith({
        id: '68954753',
        firstName: 'Alex',
        lastName: 'Vasilev',
        username: 'luckylibora',
        languageCode: 'en',
        isBot: false,
      });
    });

    it('should send welcome message', async () => {
      const reply = jest.fn();
      await botService.start({
        ...startMock,
        reply,
      } as unknown as MessageContext);

      expect(reply).toHaveBeenCalled();
    });
  });

  describe('/adminhello', () => {
    it('should send message to selected user', async () => {
      userService.getAdminById.mockResolvedValue({
        id: '68954753',
        createdAt: new Date(),
        updatedAt: new Date(),
        firstName: 'Alex',
        lastName: 'Vasilev',
        username: 'luckylibora',
        languageCode: 'en',
        isPremium: false,
        isAdmin: true,
      });

      const telegram = {
        sendMessage: jest.fn(),
      };

      await botService.adminHello({
        ...adminhelloMock,
        telegram,
      } as unknown as MessageContext);

      expect(telegram.sendMessage).toHaveBeenCalledWith(
        '68954753',
        expect.any(String),
      );
    });

    it('should do nothing if current user is not admin', async () => {
      userService.getAdminById.mockResolvedValue(null);

      const telegram = {
        sendMessage: jest.fn(),
      };

      await botService.adminHello({
        ...adminhelloMock,
        telegram,
      } as unknown as MessageContext);

      expect(telegram.sendMessage).not.toHaveBeenCalled();
    });
  });
});
