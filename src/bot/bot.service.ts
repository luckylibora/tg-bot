import { UserService } from '../users/user.service';
import { UpsertUserDto } from '../users/dto/upsert-user.dto';
import { toCamelCase } from '../core/utils/object.helpers';
import { PinoLogger } from 'nestjs-pino';
import { Command, Ctx, Start, Update } from 'nestjs-telegraf';
import { MessageContext } from './message-context.type';
import { BotConfig } from './bot.config';

@Update()
export class BotService {
  constructor(
    private userService: UserService,
    private logger: PinoLogger,
    private botConfig: BotConfig,
  ) {
    this.logger.setContext(BotService.name);
  }

  @Start()
  async start(@Ctx() ctx: MessageContext): Promise<void> {
    const { id, ...rest } = toCamelCase(ctx.message.from);
    const upsertUserDto = new UpsertUserDto({ id: id.toString(), ...rest });
    await this.userService.upsert(upsertUserDto);
    await ctx.reply('Hey', {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Click Me',
              web_app: {
                url: this.botConfig.webAppUrl,
              },
            },
          ],
        ],
      },
    });
  }

  @Command('adminhello')
  async adminHello(@Ctx() ctx: MessageContext): Promise<void> {
    const adminUser = await this.userService.getAdminById(
      ctx.from.id.toString(),
    );

    if (!adminUser) {
      this.logger.warn(`Usage /adminhello by non-admin userId=${ctx.from.id}`);
      return;
    }

    const [userId, ...rest] = ctx.args;
    const msg = rest.join(' ');
    await ctx.telegram.sendMessage(userId, msg);
  }
}
