import { Context } from 'telegraf';
import {
  Message as TelegrafMessage,
  Update as TelegrafUpdate,
  // eslint-disable-next-line import/no-unresolved
} from 'telegraf/typings/core/types/typegram';

export type MessageContext = Context<{
  message: TelegrafUpdate.New &
    TelegrafUpdate.NonChannel &
    TelegrafMessage.TextMessage;
  update_id: number;
}> & { args: string[] };
