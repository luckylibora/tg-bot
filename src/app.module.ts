import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { CoreModule } from './core/core.module';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { BotModule } from './bot/bot.module';
import { UserModule } from './users/user.module';

@Module({
  imports: [CoreModule, BotModule, UserModule],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ whitelist: true }),
    },
  ],
})
export class AppModule {}
