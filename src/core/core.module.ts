import { Module } from '@nestjs/common';
import { configModule } from './config/config.module';
import { loggerModule } from './logger/logger.module';
import { dbModule } from './db/db.module';
import { telegrafModule } from './telegraf/telegraf.module';

@Module({
  imports: [configModule, loggerModule, dbModule, telegrafModule],
})
export class CoreModule {}
