import { LoggerModule } from 'nestjs-pino';
import { Options } from 'pino-http';
import { LoggerConfig } from '../config/logger.config';

export const loggerModule = LoggerModule.forRootAsync({
  useFactory: (loggerConfig: LoggerConfig) => {
    const pinoHttp: Options = {
      level: loggerConfig.level,
    };

    if (loggerConfig.usePretty) {
      pinoHttp.transport = { target: 'pino-pretty' };
    }

    return {
      pinoHttp,
    };
  },
  inject: [LoggerConfig],
});
