import _ from 'lodash';

export type SnakeToCamelCase<S> = S extends `${infer T}_${infer U}`
  ? `${T}${Capitalize<SnakeToCamelCase<U>>}`
  : S;

export type SnakeToCamelCaseObject<T> = {
  [P in keyof T as SnakeToCamelCase<P>]: T[P];
};

export function toCamelCase<T extends object>(obj: T): SnakeToCamelCase<T> {
  return _.mapKeys(obj, (v, k) => _.camelCase(k)) as SnakeToCamelCase<T>;
}
