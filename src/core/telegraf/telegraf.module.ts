import { TelegrafModule, TelegrafModuleOptions } from 'nestjs-telegraf';
import { TelegrafConfig } from '../config/telegraf.config';
import { TELEGRAF_WEBHOOK_PATH } from './telegraf.consts';

export const telegrafModule = TelegrafModule.forRootAsync({
  useFactory: (telegrafConfig: TelegrafConfig) => {
    const options: TelegrafModuleOptions = { token: telegrafConfig.token };

    if (telegrafConfig.webhookDomain) {
      options.launchOptions = {
        webhook: {
          domain: telegrafConfig.webhookDomain,
          path: TELEGRAF_WEBHOOK_PATH,
        },
      };
    }

    return options;
  },
  inject: [TelegrafConfig],
});
