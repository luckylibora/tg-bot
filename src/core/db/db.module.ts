import { TypeOrmModule } from '@nestjs/typeorm';
import { DbConfig } from '../config/db.config';
import { addTransactionalDataSource } from 'typeorm-transactional';
import { DataSource, DataSourceOptions } from 'typeorm';

export const dbModule = TypeOrmModule.forRootAsync({
  inject: [DbConfig],
  useFactory: (dbConfig: DbConfig) => ({
    type: 'postgres',
    url: dbConfig.url,
    synchronize: false,
    entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
    migrations: [__dirname + '/../../migrations/*{.ts,.js}'],
    migrationsRun: true,
  }),
  // eslint-disable-next-line @typescript-eslint/require-await
  dataSourceFactory: async (options?: DataSourceOptions) => {
    if (!options) {
      throw new Error('Invalid options passed');
    }

    return addTransactionalDataSource(new DataSource(options));
  },
});
