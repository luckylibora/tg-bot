import { IsBoolean, IsInt, IsOptional, IsString, IsUrl } from 'class-validator';

export class EnvConfig {
  @IsOptional()
  @IsBoolean()
  LOGGER_PRETTY: boolean;

  @IsOptional()
  LOGGER_LEVEL: string;

  @IsString()
  BOT_API_TOKEN: string;

  @IsString()
  @IsOptional()
  BOT_WEBHOOK_DOMAIN: string;

  @IsOptional()
  @IsInt()
  PORT: number;

  @IsString()
  DB_URL: string;

  @IsUrl()
  WEB_APP_URL: string;
}
