import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DbConfig {
  constructor(private configService: ConfigService) {}

  get url(): string {
    return this.configService.get('DB_URL')!;
  }
}
