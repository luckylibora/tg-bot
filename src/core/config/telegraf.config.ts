import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Optional } from '../types/optional.type';

@Injectable()
export class TelegrafConfig {
  constructor(private configService: ConfigService) {}

  get token(): string {
    return this.configService.get<string>('BOT_API_TOKEN')!;
  }

  get webhookDomain(): Optional<string> {
    return this.configService.get<string>('WEBHOOK_DOMAIN');
  }
}
