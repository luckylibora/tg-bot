import { ConfigModule } from '@nestjs/config';
import { validateConfig } from './config.helpers';
import { LoggerConfig } from './logger.config';
import { HttpConfig } from './http.config';
import { DbConfig } from './db.config';
import { TelegrafConfig } from './telegraf.config';

export const configModule = ConfigModule.forRoot({
  isGlobal: true,
  validate: validateConfig,
});

const additionalConfigProviders = [
  LoggerConfig,
  HttpConfig,
  DbConfig,
  TelegrafConfig,
];

configModule.providers?.push(...additionalConfigProviders);
configModule.exports?.push(...additionalConfigProviders);
