import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class LoggerConfig {
  constructor(private configService: ConfigService) {}

  get usePretty(): boolean {
    return this.configService.get('LOGGER_PRETTY', false);
  }

  get level(): string {
    return this.configService.get('LOGGER_LEVEL', 'debug');
  }
}
