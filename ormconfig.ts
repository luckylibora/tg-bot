import 'dotenv/config';
import { DataSource } from 'typeorm';

module.exports = new DataSource({
  type: 'postgres',
  url: process.env.DB_URL,
  entities: ['src/**/*.entity{.ts,.js}'],
  migrations: ['src/migrations/*{.ts,.js}'],
});
