## Configuration

Create `.env` file from `.env.example`
```bash
cp .env.example .env
```

Change `.env` if it's necessary

## Installation

```bash
npm ci
```

## Running the app

```bash
# watch+debug mode for local development
npm run start:debug
```

```bash
# production mode
npm run build
npm run start:prod
```

## Test

```bash
# running tests
npm run test
```

```bash
# test coverage
npm run test:cov
```

## Swagger

Swagger UI is available on [/swagger](http://localhost:3000/swagger) 

## Running with Docker 

For local development docker-compose can be used, it runs backend in watch and debug modes
```bash
docker-compose up
```

For building an image for production 
```bash
docker build -t tg-test .
```

### Tests
After starting docker compose, it can be used for running tests as well
```bash
docker exec tg-test_backend_1 npm test
```

For running tests with coverage
```bash
docker exec tg-test_backend_1 npm run test:cov
```
